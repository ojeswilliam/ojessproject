#include <iostream>
using namespace std;

int main() {
    string namaKaryawan;
    char golongan;
    string status;

    cout << "Nama Karyawan: ";
    cin >> namaKaryawan;
    cout << "Golongan (A/B): ";
    cin >> golongan;
    cout << "Status (Nikah/Belum): ";
    cin >> status;

    int gajiPokok, tunjangan;
    double prosentaseIuran, potonganIuran, gajiBersih;

    if (golongan == 'A') {
        gajiPokok = 200000;
        if (status == "Nikah") {
            tunjangan = 50000;
        } else {
            tunjangan = 25000;
        }
    } else if (golongan == 'B') {
        gajiPokok = 350000;
        if (status == "Nikah") {
            tunjangan = 75000;
        } else {
            tunjangan = 60000;
        }
    } else {
        cout << "Golongan tidak valid" << endl;
        return 1; // Exit with an error code
    }

    if (gajiPokok + tunjangan <= 300000) {
        prosentaseIuran = 0.05;
    } else {
        prosentaseIuran = 0.10;
    }

    potonganIuran = (gajiPokok + tunjangan) * prosentaseIuran;
    gajiBersih = gajiPokok + tunjangan - potonganIuran;

    cout << "Gaji Pokok : Rp. " << gajiPokok << endl;
    cout << "Tunjangan : Rp. " << tunjangan << endl;
    cout << "Potongan Iuran: Rp. " << potonganIuran << endl;
    cout << "Gaji Bersih : Rp. " << gajiBersih << endl;

    return 0;
}
