#include <iostream>
#include <stdlib.h>
#include <iomanip>

using namespace std;

int main()
{
    system("clear");

    int JumlahAnak = 2;
    float GajiKotor = 0.0, Tunjangan = 0.0, PersenTunjangan = 0.0, Gajibersih = 0.0, PersenPotongan = 0.0;
    float Potongan = 0.0;

    PersenTunjangan = 0.2;
    PersenPotongan = 0.00;
    cout << "Gaji Kotor ? "; cin >> GajiKotor;
    cout << "Jumlah Anak ? "; cin >> JumlahAnak;
    if (JumlahAnak > 2)
    {
        PersenTunjangan = 0.3;
        
    }
    if (GajiKotor > 50)
{
    PersenPotongan = 0.07;
}

    Tunjangan = PersenTunjangan * GajiKotor;
    Potongan = PersenPotongan * GajiKotor;
    Gajibersih = GajiKotor + Tunjangan - Potongan;
    cout << "Besar Tunjangan = Rp " << setprecision(2) << Tunjangan << endl;
    cout << "Besar Gajibersih = RP " << setprecision(2) << Gajibersih << endl;
    cout << "Besar Potongan = RP " << setprecision(2) << Potongan << endl;
    

    return 0;
}

