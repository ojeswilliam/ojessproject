#include <iostream>
#include <iomanip>

using namespace std;

int main() {
    int suhuAwal = 1;
    int suhuAkhir = 30;
    int langkah = 1;

    cout << "-----------------------------" << endl;
    cout << "Celcius\t\tFahrenheit\tKelvin" << endl;
    cout << "-----------------------------" << endl;

    for (int celcius = suhuAwal; celcius <= suhuAkhir; celcius += langkah) {
        double fahrenheit = (celcius * 9.0 / 5.0) + 32;
        double kelvin = celcius + 273.15;
     cout << "|" << setw(8) << celcius << "|" << "\t" << setw(12) << fahrenheit << "|" << "\t" << setw(8) << kelvin << endl;

    }
    cout << "-------------------------------" << endl;
    return 0;

}