#include <iostream>
#include <iomanip>

using namespace std;

int main() {
    int suhuAwal;
    int suhuAkhir;
    int step;

    cout << "Masukkan suhu awal : ";
    cin >> suhuAwal;

    cout << "Masukkan suhu akhir : ";
    cin >> suhuAkhir;

    cout << "Masukkan step : ";
    cin >> step;

    cout << "-----------------------------" << endl;
    cout << "Celcius\t\tFahrenheit\tKelvin" << endl;
    cout << "-----------------------------" << endl;

    for (int celcius = suhuAwal; celcius <= suhuAkhir; celcius += step) {
        double fahrenheit = (celcius * 9.0 / 5.0) + 32;
        double kelvin = celcius + 273.15;
     cout << "|" << setw(8) << celcius << "|" << "\t" << setw(12) << fahrenheit << "|" << "\t" << setw(8) << kelvin << endl;

    }
    cout << "-------------------------------" << endl;
    return 0;

}