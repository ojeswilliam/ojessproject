#include <iostream>
using namespace std;

int main() {
    int i, j;
    cout << "Masukkan nilai i: ";
    cin >> i;
    cout << "Masukkan nilai j: ";
    cin >> j;

    int hasilPertambahan = i + j;
    int hasilPengurangan = i - j;
    int hasilPerkalian = i * j;
    int hasilPembagian = i / j;
    int hasilModulo = i % j;

    cout << "--------------------------" << endl;
    cout << "| Operasi " << i << " + " << j << " = " << hasilPertambahan << " |" << endl;
    cout << "| Operasi " << i << " - " << j << " = " << hasilPengurangan << " |" << endl;
    cout << "| Operasi " << i << " * " << j << " = " << hasilPerkalian << " |" << endl;
    cout << "| Operasi " << i << " / " << j << " = " << hasilPembagian << " |" << endl;
    cout << "| Operasi " << i << " % " << j << " = " << hasilModulo << " |" << endl;
    cout << "--------------------------" << endl;

    return 0;
}
